[Клиентская документация](https://gitlab.com/xquanto/docs#%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82%D1%81%D0%BA%D0%B0%D1%8F-%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F)

[[_TOC_]]

<!-- * Индексы
	* Индексы на валюту
		- Ставка овернайт валюту .N
	* Индексы на кросс-пару
		- Взвешенный индекс на основную кросс-пару .I
		- Взвешенный индекс на композитную кросс-пару .I
		- Индекс исторической волатильности .H
	* Индексы на контракт
		- Торговый индекс (индекс последней сделки) контракта
		- Индекс открытого интереса .O
		- Индекс ставка премиум .P
		- Индекс справедливой цены .F
		- Индекс маркировки для фьючерса .M
		- Индекс маркировки для бессрочного контракта .M
		- Индекс маркировки для опциона .M
		- Индекс ожидаемой волатильности для опциона .V
		- Индекс ожидаемой волатильности для фьючерса .U
		- Индекс рефинансирования бессрочного контракта .R
	* Форвардные индексы
		- Индекс форвардной ставки .W
	* Стабилизация значений индексов
-->

## Индексы

### Индексы на валюту

#### Ставка овернайт валюту .N

Каждая валюта характеризуется своей краткосрочной процентной ставкой овернайт,
которая для фиатных валют задаётся центральным банком, а для криптовалют определяется
прогнозированием краткосрочных ставок с помощью [модели BDT](http://pluto.mscc.huji.ac.il/~mswiener/research/Benninga73.pdf)
В качестве входных данных - динамика форвардных ставок на эту валюту и разные даты.

```math
I_n = F_{bdt}(I_w^*)
```

$`I_n`$ - идекс ставки овернайт  
$`I_w^*`$ - все форвардные индексы на одну валюту, и их динамика  

*Индекс используется в качестве ставки для расчёта индекса маркировки бессрочных контрактов.*

*Для стабилизации результата действует ограничение на изменение в размере 1 процентного пункта в 8-ми часовой интервале.*

В разработке: ~~Алгоритм BDT~~


### Индексы на кросс-пару

#### Взвешенный индекс на основную кросс-пару .I

Взвешенный индекс на основную кросс-пуру, рассчитывается как среднее значение справедливых цен кросс-пары на других площадках.
Справедливая цена моделируется как средня цена между модельной сделкой на покупку, эквивалентной 10000 USD,
и модельной сделкой на продажу, эквивалентной 10000 USD. Для каждой кросс-пары определён список площадок,
по которым берётся среднее значение.

BTCUSD:
* BTCUSD.E.BTMP - внешний индекс BTCUSD, биржа Bitstamp
* BTCUSD.E.KRKN - внешний индекс BTCUSD, биржа Kraken
* BTCUSD.E.GDAX - внешний индекс BTCUSD, биржа Coinbase
* BTCUSD.E.GMIN - внешний индекс BTCUSD, биржа Gemini

ETHUSD:
* ETHUSD.E.BTMP - внешний индекс ETHUSD, биржа Bitstamp
* ETHUSD.E.KRKN - внешний индекс ETHUSD, биржа Kraken
* ETHUSD.E.GDAX - внешний индекс ETHUSD, биржа Coinbase
* ETHUSD.E.BTFIN - внешний индекс ETHUSD, биржа Bitfinex

LTCUSD:
* LTCUSD.E.BTMP - внешний индекс LTCUSD, биржа Bitstamp
* LTCUSD.E.GDAX - внешний индекс LTCUSD, биржа Coinbase
* LTCUSD.E.BTFIN - внешний индекс LTCUSD, биржа Bitfinex
* LTCUSD.E.GMIN - внешний индекс LTCUSD, биржа Gemini

XRPUSD:
* XRPUSD.E.BTMP - внешний индекс XRPUSD, биржа Bitstamp
* XRPUSD.E.KRKN - внешний индекс XRPUSD, биржа Kraken
* XRPUSD.E.BTFIN - внешний индекс XRPUSD, биржа Bitfinex
* XRPUSD.E.LQUID - внешний индекс XRPUSD, биржа Liquid

USDTUSD:
* USDTUSD.E.KRKN - внешний индекс USDTUSD, биржа Kraken
* USDTUSD.E.BTFIN - внешний индекс USDTUSD, биржа Bitfinex

```math
I_i = I_{ex_1} + I_{ex_2}
```

$`I_i`$ - взвешенный индекс основной кросс-пары  
$`I_{ex_i}`$ - внешние индексы справедливой цены на основную кросс-пару  

*Индекс играет роль спот, и используется в качестве значения стоимости базового актива в расчёте индекса маркировки и греков,
расчёте ожидаемой волатильности для опционов, основным критерием по отношению к ликвидационным ценам,
и в качестве стоимости расчёта при экспирации фьючерсов.*


#### Взвешенный индекс на композитную кросс-пару .I

Взвешенный индекс **.I** композитную кросс-пару рассчитывается как отношение взвешенных индексов:
первой валюты к доллару и второй валюты к доллару. 

```math
I_i^{cur_i/cur_j} = \frac{I_i^{cur_i/usd}} {I_i^{cur_j/usd}}
```

$`I_i^{cur_i/cur_j}`$ - взвешенный индекс на композитную кросс-пару  
$`I_i^{cur_i/usd}`$ - взвешенный индексы на основные кросс-пары  

*Индекс играет роль спот, и используется в качестве значения стоимости базового актива в расчёте индекса маркировки и греков,
расчёте ожидаемой волатильности для опционов, основным критерием по отношению к ликвидационным ценам,
и в качестве стоимости расчёта при экспирации фьючерсов.*

[Композитная кросс-пара](#cross-composite)


#### Индекс исторической волатильности .H

Для моделирования других индексов может быть необходимо значение исторической волатильности на кросс-пару.
Значение индекса рассчитывается как стандартное отклонение исторических данных на дневном таймфрейме:

```math
E = \frac{1}{30} \sum_{i < 30} {\ln(c_i / c_{i+1})}
```

```math
I_h = \sqrt{\frac{365}{30}} \sum_{i < 30} \left( \ln(c_i / c_{i+1}) - E \right)^2
```

$`I_h`$ - идекс исторической волатильности на кросс-пару

*Индекс носит больше информативный характер, и используется в качестве начальных 
значений (дефолтных, при отсутствие возможности получить точное значение),
для индекса ожидаемой волатильности для фьючерса.*

*Для стабилизации результата действует ограничение на изменение в размере 20 процентных пунктов в 8-ми часовой интервале.*


### Индексы на контракт

#### Торговый индекс (индекс последней сделки) контракта

Торговый индекс обозначается без суффикса и совпадает с символом контракта. 
Индекс формируется из цены последней сделки в рамкоах биржевого тика.
Вместе с торговым индексом для заданных таймфреймов рассчитывается значение объёма,
которое формируется как кумулятивное количество контрактов в сделках за определённый интервал таймфрейма.

*Индекс не используется для расчётов, но играет важную роль для принятия решения для клиентов.*


#### Индекс открытого интереса .O

Индекс открытого интереса рассчитывается как кумулятивное значение изменений открытого интереса
в каждом биржевом тике, и отражает суммарное количество всех длинных контрактов на всех аккаунтах
(количество коротких должно обладать симметричным свойством)

*Индекс носит информативный характер*


#### Индекс ставка-премиум .P

Индекс ставка-премиум выступает в качестве стабилизатора для расчёта индекса справедливой цены.

В основе индекса лежит базовое значение справедливой цены,
которое определяется как средняя цена между модельной сделкой на покупку,
эквивалентной 10000 USD, и модельной сделкой на продажу, эквивалентной 10000 USD:

```math
I_f^{base} = \frac {A_b + A_s} {2}
```

$`I_f^{base}`$ - базовое значение справедливой цены  
$`A_b`$ - средняя цена при покупки на сумму 10000 долларов  
$`A_s`$ - средняя цена при продаже на сумму 10000 долларов  

Данное значение фиксируется, в виде ставки, по отношению к индексу маркировки:

```math
I_p = \frac{1} {t} \cdot \ln \frac{I_f^{base}} {I_m}
```

$`I_p`$ - индекс ставка премиум  
$`t`$ - срок до экспирации фьючерса, не менее 8 часов  

*Для стабилизации результата действует ограничение на изменение в размере 8 процентных пунктов в 8-ми часовой интервале.*

*Индекс используется для вычисления индекса справедливой цены и индекса рефинансирования*


#### Индекс справедливой цены .F

Индекс справедливой цены вычисляется через стабилизирующий индекс ставка-премиум из индекса маркировки:

```math
I_f = F^{fut}(I_m,I_p,t)
```

$`I_f`$ - индекс справедливой цены  
$`t`$ - срок до экспирации фьючерса, не менее 8 часов  

*Индекс используется для расчёта форвардных ставок, оценки ожидаемой волатильности опциона,
и в качестве второй цены для сделки в композитном контракте.*


#### Индекс маркировки для фьючерса .M

Индекс маркировки для фьючерса рассчитывается:

```math
I_m = F^{fut}(s,r,t)
```

$`I_m`$ - индекс маркировки на контракт  
$`F^{fut}(s,r,t)`$ - функция зависимости стоимости фьючерса от базового актива  
$`s`$ - значение взвешенного индекса на кросс-пару $`I_i`$  
$`r`$ - разница форвардных ставок на валюты $`I^{cur_2}_w-I^{cur_1}_w`$  
$`t`$ - срок до экспирации фьючерса  

*Индекс используется для вычисления нереализованной прибыли, оценки гарантийного обеспечения,
и для расчёта индекса ожидаемой волатильности фьючерса.*

[Зависимость стоимости фьючерса от базового актива](greek.md#greek-future)


#### Индекс маркировки для бессрочного контракта .M

Индекс маркировки для бессрочного контракта рассчитывается:

```math
I_m = F^{fut}(s,r,t)
```

$`I_m`$ - индекс маркировки на контракт  
$`F^{fut}(s,r,t)`$ - функция зависимости стоимости фьючерса от базового актива  
$`s`$ - значение взвешенного индекса на кросс-пару $`I_i`$  
$`r_z`$ - разница ставок овернайт на валюты $`I^{cur_2}_n-I^{cur_1}_n`$  
$`t_z`$ - время до окончания 8-ми часового интервала  

*Индекс используется для вычисления нереализованной прибыли, и для расчёта индекса премиум.*

[Зависимость стоимости фьючерса от базового актива](greek.md#greek-future)


#### Индекс маркировки для опциона .M

Индекс маркировки опционного контракта расчитывается:

```math
I_m = F^{option}(s,k,v,t,u)
```

$`I_m`$ - индекс маркировки на контракт  
$`F^{option}(s,k,v,t,u)`$ - функция зависимости стоимости опциона пут или колл от базового актива  
$`s`$ - значение взвешенного индекса на кросс-пару $`I_i`$  
$`k`$ - страйк опциона  
$`r`$ - разница форвардных ставок на валюты $`I^{cur_2}_w-I^{cur_1}_w`$  
$`v`$ - ожидаемая волатильность на фьючерсный контракт $`I_u`$  
$`u`$ - срок до экспирации опциона  
$`t`$ - срок до экспирации фьючерса  

*Индекс используется для вычисления нереализованной прибыли, оценки гарантийного обеспечения,
и для расчёта индекса ожидаемой волатильности опциона.*

[Зависимость стоимости опциона от базового актива](greek.md#greek-option)


#### Индекс ожидаемой волатильности для опциона .V

Индекс ожидаемой волатильности опционного контракта расчитывается, как решение обратной задчи:

```math
I_f = F^{option}(s,k,v,t,u)
```

$`I_f`$ - индекс справедливой цены на контракт  
$`F^{option}(s,k,v,t,u)`$ - функция зависимости стоимости опциона пут или колл от базового актива  
$`s`$ - значение взвешенного индекса на кросс-пару $`I_i`$  
$`k`$ - страйк опциона  
$`r`$ - разница форвардных ставок на валюты $`I^{cur_2}_w-I^{cur_2}_w`$  
$`v`$ - ожидаемая волатильность на опционный контракт $`I_v`$, как искомый параметр  
$`u`$ - срок до экспирации опциона  
$`t`$ - срок до экспирации фьючерса  

*Индекс используется для вычисления ожидаемой волатильности фьючерса*

*Для стабилизации результата действует ограничение на изменение в размере 10 процентых пунктов в 8-ми часовой интервале.*


#### Индекс ожидаемой волатильности для фьючерса .U

Индекс ожидаемой волатильности фьючерсного контракта расчитывается,
как взвешенные значения ожидаемой волатильности на опционы:

```math
I_u = \sum_{tk} I_v(t,k)w(t,k)
```

$`I_u`$ - индекс ожидаемой волатильности на фьючерс  
$`I_v(t,k)`$ - индекс ожидаемой волатильности на опцион с экспирацией $`t`$ и страйком $`k`$  
$`w(t,k)`$ - весовой коэффициент, в котором наибольший вес имеют опционы около денег, и меньшим сроком экспирации  

*Индекс используется для вычисления индекса маркировки опциона*

*Для стабилизации результата действует ограничение на изменение в размере 10 процентынх пунктов в 8-ми часовой интервале.*


#### Индекс рефинансирования бессрочного контракта .R

Индекс рефинансирования бессрочного контракта расчитывается как разница ставок овернайт:

<!-- ```math
I_r = 365 \cdot 3 \cdot \mathrm{TWAP}(I_p)
```

$`I_r`$ - индекс рефинансирования, как процентная ставка в годовом выражении, для 8-ми часового интервала  
$`I_p`$ - индекс премиум для бессрочного контракта  
$`\mathrm{TWAP}`$ - среднее значение в данном итервале  

 -->

```math
I_r = I^{cur_2}_n-I^{cur_1}_n
```

$`I_r`$ - индекс рефинансирования, как процентная ставка в годовом выражении, для 8-ми часового интервала  
$`I^{cur}_n`$ - ставка овернайт на валюту  

*Индекс используется для проведения рефинансирования по заданной ставке*


### Индексы на форвард

#### Индекс форвардной ставки .W

Форвардный индекс характеризуется датой, на которую распространяется ставка, и самой валютой.
Поиск ставок ведётся на одну дату и одновременно на все валюты,
методом наименьших квадратов разницы между индексами справедливых цен и
и оценкой фьючерса, которая используется для вычисления индекса маркировки.
Таким образом задача оптимизации состоит из целевой функции:

```math
W = \sum_{ij} \left(I_f^{cur_i/cur_j} - F^{fut}(I_i^{cur_i/cur_j}, r_{cur_j} - r_{cur_i}, t) \right)^2 \rarr \min
```

и ограниченй:
```math
\begin{aligned}
r_{usd} = I_n \\
r_{cur} > -0.02
\end{aligned}
```

$`W`$ - квадратичная целевая функция оптимизации  
$`I_f^{cur_i/cur_j}`$ - индекс справедливой цены контракта  
$`F^{fut}`$ - функция зависимости стоимости фьючерса от базового актива  
$`I_i^{cur_i/cur_j}`$ - взвешенной индекс кросс-пары  
$`r_{cur}`$ - форвардные ставки на валюту, как искомый параметр $`I_w^{cur}`$  
$`t`$ - время до экспирации, одинаковое для всех контрактов  
$`I_n`$ - индекс ставки овернайт на фиатные валты  

*Индекс используется расчёта индекса маркировки фьючерсов и опционов, индекса овернайт,
индекса ожидаемой волатильности для опционов*

*Для стабилизации результата действует ограничение на изменение в размере 1 процентного пункта в 8-ми часовой интервале.*

### Стабилизация значений индексов

Для промежуточных индексов, при наличии указания в описании, используется стабилизирующая функция:

```math
\mathrm{clamp}(x, y, d) = \begin{cases}
x + \min(d, |y-x|), &y \geqslant x \\
x - \min(d, |y-x|), &y < x
\end{cases}
```

$`\mathrm{clamp}(x, y, d)`$ - стабилизирующая функция  




