[Клиентская документация](https://gitlab.com/xquanto/docs#%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82%D1%81%D0%BA%D0%B0%D1%8F-%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F)

[[_TOC_]]

<!-- * Спецификация бессрочного контракта (ETHBTC-ZZ)
 -->

## Спецификация бессрочного контракта (ETHBTC-ZZ)

|Наименование											|Значение								|
|-------------------------------------------------------|---------------------------------------|
|Контракт 												|ETHBTC-ZZ								|
|Тип 													|Бессрочный фьючерсный контракт 		|
|Дата экспирации										|Бессрочный								|
|Индекс маркировки контракта							|ETHBTC-ZZ.M (`ETHBTC-ZZ.M`) 			|
|Индекс справедливой цены контракта						|ETHBTC-ZZ.F (`ETHBTC-ZZ.F`) 			|
|Индекс премиум контракта								|ETHBTC-ZZ.P (`ETHBTC-ZZ.P`%AYR) 		|
|Индекс открытого интереса контракта					|ETHBTC-ZZ.O (`ETHBTC-ZZ.O`) 			|
|Базовый актив											|ETHBTC 								|
|Тип базового актива									|композитный 							|
|Дополнительный индекс в расчёте P&L 					|BTCUSD-ZZ.F (`BTCUSD-ZZ.F`) 			|
|Взвешенный индекс на базовый актив						|ETHBTC.I (`ETHBTC.I`) 					|
|Индекс исторической волатильности на базовый актив 	|ETHBTC.H (`ETHBTC.H`) 					|
|Базовая валюта базового актива							|ETH 									|
|Индекс овернайт базовой валюты							|ETH.N (`ETH.N`%AYR) 					|
|Форвардный индекс базовой валюты						|ETH-ZZ.W (`ETH-ZZ.W`%AYR) 				|
|Вторая валюта базового актива							|BTC 									|
|Индекс овернайт второй валюты							|BTC.N (`BTC.N`%AYR) 					|
|Форвардный индекс второй валюты						|BTC-ZZ.W (`BTC-ZZ.W`%AYR) 				|
|Индекс рефинансирования контракта						|ETHBTC-ZZ.R (`ETHBTC-ZZ.R`%AYR) 		|
|Период рефинансирования контракта						|каждые 8 часов						 	|
|Ставка рефинансирования контракта в текущем периоде	|`fixed ETHBTC-ZZ.R`				 	|
|Время до окончания текущего периода рфеинансирования	|`front TODO` 							|
|Расчётная валюта										|USD 									|
|Размер контракта										|100 USD 								|
|Начальное ГО											|1.2 USD 								|
|Поддерживающее ГО										|1.0 USD 								|
|Исполнение расчётов									|отсутствует 							|
|Минимальное изменение цены								|`0.0000025`	 						|
|Автоделивередж                                         |есть                                   |
|Комиссия тэйкера                                       |`0.1` USD                              |
|Комиссия мэйкера                                       |`0.0` USD                              |
|Комиссия ликвидации                                    |`0.5` USD                              |
|Заморожены торги                                       |нет                                    |
|Максимальная заявка									|1000 контрактов 						|
|Базовый лимит позиции									|10000 контрактов 						|

## Спецификация срочного контракта (ETHBTC-U1)

|Наименование											|Значение								|
|-------------------------------------------------------|---------------------------------------|
|Контракт 												|ETHBTC-U1								|
|Тип 													|Срочный фьючерсный контракт 			|
|Дата экспирации										|`17.09.2021`							|
|Время до экспирации									|`front TODO` 							|
|Цена экспирации										|`expiration_price` 					|
|Вторая цена экспирации									|`expiration_price_second` 				|
|Время до экспирации									|`front TODO` 							|
|Индекс маркировки контракта							|ETHBTC-U1.M (`ETHBTC-U1.M`) 			|
|Индекс справедливой цены контракта						|ETHBTC-U1.F (`ETHBTC-U1.F`) 			|
|Индекс премиум контракта								|ETHBTC-U1.P (`ETHBTC-U1.P`%AYR) 		|
|Индекс открытого интереса контракта					|ETHBTC-U1.O (`ETHBTC-U1.O`) 			|
|Базовый актив											|ETHBTC 								|
|Тип базового актива									|композитный 							|
|Дополнительный индекс в расчёте P&L 					|BTCUSD-U1.F (`BTCUSD-U1.F`) 			|
|Взвешенный индекс на базовый актив						|ETHBTC.I (`ETHBTC.I`) 					|
|Индекс исторической волатильности на базовый актив 	|ETHBTC.H (`ETHBTC.H`) 					|
|Базовая валюта базового актива							|ETH 									|
|Индекс овернайт базовой валюты							|ETH.N (`ETH.N`%AYR) 					|
|Форвардный индекс базовой валюты						|ETH-U1.W (`ETH-U1.W`%AYR) 				|
|Вторая валюта базового актива							|BTC 									|
|Индекс овернайт второй валюты							|BTC.N (`BTC.N`%AYR) 					|
|Форвардный индекс второй валюты						|BTC-U1.W (`BTC-U1.W`%AYR) 				|
|Расчётная валюта										|USD 									|
|Размер контракта										|100 USD 								|
|Начальное ГО											|1.2 USD 								|
|Поддерживающее ГО										|1.0 USD 								|
|Исполнение расчётов									|отсутствует 							|
|Индекс расчёта                                         |ETHBTC.I                               |
|Дополнительный индекс расчёта                          |BTCUSD.I                               |
|Минимальное изменение цены								|`0.0000025`	 						|
|Автоделивередж                                         |есть                                   |
|Комиссия тэйкера                                       |`0.1` USD                              |
|Комиссия мэйкера                                       |`0.0` USD                              |
|Комиссия ликвидации                                    |`0.5` USD                              |
|Заморожены торги                                       |нет                                    |
|Максимальная заявка									|1000 контрактов 						|
|Базовый лимит позиции									|10000 контрактов 						|

TODO: Индекс средней ожидаемой волатильности необходимо расчитывать по всем опционам


----------------------------------------------------------------------


Символ индекса на валютную пару:			BTCUSD.I
Символ индекса маркировки:				BTCUSD-ZZ.M
Символ индекса ставки рефинансирования: 		BTCUSD-ZZ.R
Интервал рефинансирования:				каждые 8 часов
Время до рефинансирования:				5 часов
Включен авто-делевериджинг:			Да
Лимит риска:						200 000
Шаг лимита:						50 000
Исполнение расчетов:					Не производятся.
Мин. изменение цены:					0.5
Макс. цена:						16 383
Макс. количество:					536 870 911
Комиссия тэйкера:					0.075%
Комиссия мейкера:					-0.025%

Мартовский 2019 фьючерсный контракт BTCUSD:
Основной тикер:					BTCUSD-H9
Дата экспирации:					15.03.2019T23:59:59.99999Z00
Расчётная валюта:					USD
Обменная валюта:					BTC
Размер контракта:					1 USD
Начальное ГО:						1.00% + комиссия за вход тейкера +
комиссия за выход тейкера
Поддерж. ГО:						0.50% + комиссия за выход тейкера +
ставка финансирования
Символ индекса на валютную пару:			BTCUSD.I
Символ индекса маркировки:				BTCUSD-H9.M
Включен авто-делевериджинг:			Да
Лимит риска:						200 000
Шаг лимита:						50 000
Исполнение расчетов:					Не производятся.
Мин. изменение цены:					0.5
Макс. цена:						1 000 000
Комиссия тэйкера:					0.075%
Комиссия мейкера:					-0.025%


Мартовский 2019 опционный контракт колл 4500 на BTCUSD:
Основной тикер:					BTCUSD-4500С9
Дата экспирации:					14.03.2019
Страйк:						4500
Тип опциона:						Колл
Базовый актив:					BTCUSD-H9
Расчётная валюта базового актива:			USD
Обменная валюта базового актива:			BTC
Размер контракта базового актива:			1 USD
Начальное ГО продавца:				TODO
Поддерж. ГО продавца:				TODO
Включен авто-делевериджинг:			Да
Лимит риска:						TODO
Шаг лимита:						TODO
Исполнение расчетов:					Поставочный
Мин. изменение цены:					5
Макс. цена:						1 000 000
Комиссия тэйкера:					0.25%
Комиссия мейкера:					-0.05%

Бессрочный композитный фьючерсный контракт LTCBTC:
Основной тикер:					LTCBTC-ZZ
Дата экспирации:					Бессрочный
Первый контракт:					LTCUSD-ZZ
Второй контракт:					BTCUSD-ZZ
Расчётная валюта:					USD
Обменная валюта первого контракта:			LTC
Обменная валюта второго контракта:			BTC
Размер контракта:					1 USD
Начальное ГО:						2.50% + комиссия за вход тейкера +
комиссия за выход тейкера
Поддерж. ГО:						1.25% + комиссия за выход тейкера +
ставка финансирования
Символ индекса маркировки на второй контракт:	BTCUSD-ZZ.M
Символ индекса на валютную пару:			LTCBTC.I
Символ индекса маркировки:				LTCBTC-ZZ.M
Символ индекса ставки рефинансирования: 		LTCBTC-ZZ.R
Интервал рефинансирования:				каждые 8 часов
Время до рефинансирования:				5 часов
Включен авто-делевериджинг:			Да
Способ маркировки:					FairPrice
Лимит риска:						100 000
Шаг лимита:						25 000
Исполнение расчетов:					Длинная позиция по первому 
контракту и короткая по второму
Мин. изменение цены:					0.001
Макс. цена:						1 000 000
Комиссия тэйкера:					0.25%
Комиссия мейкера:					-0.05%





Ticker Root	XBTUSD
Expiry Date	Perpetual, subject to Early Settlement
Initial Margin	1.00% + Entry Taker Fee + Exit Taker Fee
Maint. Margin	0.35% + Exit Taker Fee + Funding Rate
Interest Base Symbol	.XBTBON8H
Interest Quote Symbol	.USDBON8H
Funding Premium Symbol	.XBTUSDPI8H
Funding Rate	-0.0566%
Funding Interval	every 8 hours
Next Funding	Jun 21, 2021, 11:00:00 PM
Predicted Rate	-0.0237%
Mark Price	32629.66
Auto-Deleveraging Enabled	Yes: This contract is highly speculative and manages loss through auto-deleveraging.
Mark Method	FairPrice
Fair Basis	-10.16
Fair Basis Rate	-61%
Fair Basis Calculation	The fair basis on this instrument is determined by an annualized calculation of the funding rate.
Risk Limit	200 000 mXBT
Risk Step	100 000 mXBT
Open Interest	507 214 200
24H Turnover	68 227 692,784 mXBT
Total Volume	3 129 052 078 789
Type	Settled in XBT, quoted in USD
Contract Size	1 USD (Currently 0,03065 mXBT per contract)
Settlement	This contract is perpetual and does not settle. Subject to Early Settlement.
Commission	See the Fees Reference for more details.
Minimum Price Increment	0.5 USD
Max Price	1 000 000
Max Order Quantity	10 000 000
Lot Size	100
