
## Matching Model with side independent

Для того что бы цена сделки была справедливой необходимо каждому участнику предоставить равные возможности, в зависимости от его намерений,
которые он способен формировать в конкурентной среде и ясными правилами.

За основу возьмём выбор заявок основанный по двум критериями: цена и время - это определит очередность заявок.
Ценобразование определим как сделку с наименьшими потерями для обоих сторон сделки. 

Это основные правила, интерполируем их для закрытия "белых пятен" на случаи рынка с независимым и равноправным доступом к торгам,
с двумя типами заявок: рыночной и лимитированной.

Есть стакан, сформированный и историческое время, и есть текущий тик, в котором все заявки созданные в одно время. 
Будем считать что рыночная заявка имеет наивысший приоритет по цене и первая вступает в сделку,
а по направлению рыночные друг с другом не конкурируют, значит их можно матчить независимо: покупки и продажи,
рассматривая все как равноприоритетные (по одной цене и в одно время).

Таким образом рыночная заявка - это заявка с минимальными правами, отправив в рынок она обязательно будет исполнена,
а лимитированная даёт право на сделку по фиксированной цене, или лучше.

Подробней рассмотрим лимитированные заявки и разберём ситуацию, внутри спреда (предположим 20-21) прилетают заявки в один тик: 

**Пример 1**

Orders

| ID | SIDE | QTY | PRICE |
|----|------|-----|-------|
| #1 | SELL | 150 | 20.20 |
| #2 | SELL | 100 | 20.30 |
| #3 | BUY  | 250 | 20.40 |

Они все готовы заключить между собой сделки, не затронув ткущий рынок, и в данном случае они могут
договориться по средней цена. А задача матчера - это как раз помочь договориться!

Executions

| ID | SIDE | TYPE  | QTY | PRICE | TYPE  | SIDE | ID |
|----|------|-------|-----|-------|-------|------|----|
| #3 | BUY  | TAKER | 150 | 20.30 | TAKER | SELL | #1 |
| #3 | BUY  | TAKER | 100 | 20.35 | TAKER | SELL | #2 |

Подобная задача возникает чаще всего на премаркете, и для неё есть историческое решение, 
поиска одной справедливой цены и матчинг всех заявок по ней, как например
[Auction principle](https://www.eurexchange.com/exchange-en/trade/order-book-trading/matching-principles)

Подобным образом можно поступать каждый тик, когда накапливаются перекрёстные заявки, но в рынке, 
в отличие от премаркета, есть уже существующий стакан, а аукционная цена может не вписываться в спред.
Тогда возникает вопрос, если все перекрёстные заявки одинаковые, то какие вправе взять лучшую цену из спреда?

Компромисное решение лежит в другом алгоритме как например 
[Pro rate](https://www.cmegroup.com/confluence/display/EPICSANDBOX/Supported+Matching+Algorithms#SupportedMatchingAlgorithms-Pro-Rata)
где каждая заявка в своей пропорции примет участи в освоении одной стороны спреда. Поэтому задача требует более деликатный подход.


Рассмотрим ещё пример, и сузим как раз спред предварительной заявкой SELL 50@20.30 что бы он пересёкся с аукционной ценой.

**Пример 2**

Orders book

| ID | SIDE | TIME | QTY | PRICE | QTY |  TIME   | SIDE |
|----|------|------|-----|-------|-----|---------|------|
| #1 |      |      |     | 20.30 |  50 | 12:05PM | SELL |

Orders

| ID | SIDE | QTY | PRICE |
|----|------|-----|-------|
| #2 | SELL | 120 | 20.20 |
| #3 | SELL | 100 | 20.30 |
| #4 | BUY  | 250 | 20.40 |

Executions

| ID | SIDE | TYPE  | QTY | PRICE | TYPE  | SIDE | ID |
|----|------|-------|-----|-------|-------|------|----|
| #2 | SELL | TAKER | 120 | 20.25 | TAKER | BUY  | #4 |
| #1 | SELL | MAKER |  50 | 20.30 | TAKER | BUY  | #4 |
| #3 | SELL | TAKER |  80 | 20.30 | TAKER | BUY  | #4 |


Final orders book

| ID | SIDE | TIME | QTY | PRICE | QTY |  TIME   | SIDE |
|----|------|------|-----|-------|-----|---------|------|
| #3 |      |      |     | 20.30 |  20 | 12:15PM | SELL |

Ожидаемый результат книги заявок, а порядок сделок такой что ушла заявка которая была в книге,
вместо неё по той же цене встала новая, цены в сделках не хуже заявленных и в рамках спреда. 

Разберём этот пример подробней. Спред 0-20.30, покупатель #4 и продавец #2 совершают первую сделку,
при этом цены должна быть в рамках спреда и выбрана как средее между ценой покупки ограниченной сверху 20.30 и
ценой продажи 20.20. Вторая сделка совершается между тем же покупателем и старой заявкой из стакана #1 - 
цена может быть только 20.30. После чего доходит очередь до заявки #3, которая частично и становится аском нового стакана, 
а исполняется заявленной цене в 20.30.

Такой пример показывает каким образом можно придерживаясь правил модели, провести матчинг перекрёстных заявок,
с учётом существующего спреда, по ценам не хуже заявленных (заявка лимитированная), при этом соблюдена очерёдность.

Eщё один пример, в котором сильная сторона "съедает" другую, в которой изначально были выставлены заявки:

**Пример 3**

Orders book

| ID | SIDE | TIME | QTY | PRICE | QTY |  TIME   | SIDE |
|----|------|------|-----|-------|-----|---------|------|
| #1 |      |      |     | 20.30 |  50 | 12:05PM | SELL |

Orders

| ID | SIDE | QTY | PRICE |
|----|------|-----|-------|
| #2 | SELL | 120 | 20.20 |
| #3 | SELL | 100 | 20.30 |
| #4 | BUY  | 350 | 20.40 |

Executions

| ID | SIDE | TYPE  | QTY | PRICE | TYPE  | SIDE | ID |
|----|------|-------|-----|-------|-------|------|----|
| #2 | SELL | TAKER | 120 | 20.30 | TAKER | BUY  | #4 |
| #1 | SELL | MAKER |  50 | 20.30 | TAKER | BUY  | #4 |
| #3 | SELL | TAKER | 100 | 20.40 | TAKER | BUY  | #4 |


Final orders book

| ID | SIDE |  TIME   | QTY | PRICE | QTY | TIME | SIDE |
|----|------|---------|-----|-------|-----|------|------|
| #4 | BUY  | 12:15PM |  80 | 20.40 |     |      |      |


Что бы примеры переросли в модель, необходим чёткий алгоритм действий с учётом разнообразных начальных условий.

Начать матчить заявки с концов не всегда получиться, потому что во первых: цена не может быть усреднённой,
а должна вписываться в формируемый в этом тике стакан, во вторых,
мы можем столкнуться с уже зарегистрированной сделкой в стакане, для которой цена определена!

Для того что бы разрешить эти противоречия, необходимо получить оценку,
каким же будет стакан по окончанию тика, для этого рассмотрим функцию:

Кумулятивная функция (КФ) покупателя - определяет количество лотов готовых к покупке по цене лучше или текущей включительно.
Для стакана - это глубина (где на противоположной стороне, выше бида одни нули).
Что известно об этой функции? Она монотонная, дискретная, не возрастающая функция,
в бесконечности рана 0. КФ продавца - монотонная, дискретная, не убывающая, в 0 равная 0.

По ним можно спрогнозировать какой будет стакан, что даёт возможность сформировать справедливую цену при сделках.

Рассмотрим когда КФ пересекаются, в противном случае сделок нет!
```math
B = Argmax_x(K_b(x) > K_a(x)) \\
A = Argmin_x(K_a(x) > K_b(x))
```

$`B`$ - это бид будущего стакана  
$`A`$ - аск будущего стакана

Необходимо что бы были строгие равенства, тк $`K_b(x) = K_a(x)`$ - то это цена внутри спреда,
в которой нет заявок, либо офсетные заявки по одинаковой цене, которым никто не будет мешать исполниться (их может быть только две)


Теперь зная $`B`$ и $`A`$, можем начать проводить сделки для пересекающихся заявок,
усредняя их цену, но оставляя в рамках этих ограничений.
Но это ещё не всё: в какой то момент может попасться, упомянутая выше, уже зарегистрированная заявка, более того дальше они могут идти в перемешку уже зарегистрированные и новые по одной цене и одному направлению (как в примере 2). Для таких заявок действуют разные правила: уже зарегистрированная может быть сматчена на правах мэйкера, по зарегистрированной цене, а новая: с одной стороны может быть усреднена, а с другой стороны не может быть сматчена на условиях лучше чем та что старше по времени по такой же цене!

Таким образом становиться очевидно как матчить в пределах $`b`$ - $`a`$, усредняя цены новых заявок ограничивая рамками $`B`$ - $`A`$,
и как матчить за $`b`$ и за $`a`$ - совершая по фиксированной цене для всех заявок в направлении мэйкера.

Можно убедится что мэйкером в одном тике может быть тольк одна сторона,и остаётся только определить какая?
Для этого нам необходимо оценить потенциал покупателей и продавцов - это то как на них
воздействует противоположная кумулятивная функция, иными словами: 

$`Q_b = K_b(\min(a,A))`$ - потенциал покупателя  
$`Q_a = K_a(\max(b,B))`$ - потенциал продавца

Зная потенциал, расположение $`B`$, $`A`$ по отношению к текущим $`b`$, $`a`$ в стакане - мы можем выделить 5 различных ситцаций внутри тика:

1. $`Q_b > Q_a, a=A`$ (продавцы в роли мэйкера)
2. $`Q_b > Q_a, A>a`$ (продавцы в роли мэйкера с движением $`a\rightarrow A`$)
3. $`Q_b < Q_a, b=B`$ (покупатели в роли мэйкера)
4. $`Q_b < Q_a, B<b`$ (покупатели в роли мэйкера с движением $`b\leftarrow B`$)
5. $`Q_b = Q_a`$ (сделки не затрагивают спред)

